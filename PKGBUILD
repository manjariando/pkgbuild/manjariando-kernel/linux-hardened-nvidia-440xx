# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

_linuxprefix=linux-hardened
_extramodules=extramodules-5.14-hardened
# don't edit here
pkgver=440.100_5.14.21.hardened1_1

_nver=440
# edit here for new version
_sver=100
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-${_nver}xx-utils=${_pkgver}")
groups=("$_linuxprefix-extramodules")
provides=("${_pkgname}=${_pkgver}" "nvidia-${_nver}xx-modules=${_pkgver}")
conflicts=("$_linuxprefix-nvidia" "$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx"
           "$_linuxprefix-nvidia-430xx" "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-450xx" "$_linuxprefix-nvidia-455xx"
           "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        "license.patch" "kernel-5.8.patch" "kernel-5.9.patch" "kernel-5.10.patch"
        "kernel-5.11.patch" "kernel-5.14.patch")
sha256sums=('03a96570cb141831d22f8d1866d3211f051d1444cd55753e0fe12eedf3e60450'
            'd6a6289ad4aa675033ec0a2ef4f808605fdcfbdbb319b78fff3f84cd7e42d988'
            '884878694c63cf053348bd808341e503c3008565aa6243069ebe9944f6261236'
            '2c062a3d0e5a8780afac2186677884a0a62769d49340d50c958c485a1f2e08ba'
            'b71afd4610887ffb4b828930e4f17aac228651053fda0a01807ed2801732ce6f'
            '15bbd86c65b1dc4637b5ecb8ad572e3e4e491cbefcd91036537b645a5d9a5b2d'
            '2f5222ce0f1bba1a7804a5b29083de4fb4ae215a96c0dd6b96cd213dde3e61a6')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here
    # original patches https://www.if-not-true-then-false.com/2020/inttf-nvidia-patcher
    # Fix compile problem with license
    msg2 "PATCH: license"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/license.patch

    # Fix compile problem with 5.8
    msg2 "PATCH: kernel-5.8"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.8.patch

    # Fix compile problem with 5.9
    msg2 "PATCH: kernel-5.9"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.9.patch

    # Fix compile problem with 5.10
    msg2 "PATCH: kernel-5.10"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.10.patch

    # Fix compile problem with 5.11
    msg2 "PATCH: kernel-5.11"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.11.patch

    # Fix compile problem with 5.14
    # Based on https://gist.github.com/joanbm/144a965c36fc1dc0d1f1b9be3438a368
    msg2 "PATCH: kernel-5.14"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.14.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-modeset.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-modeset.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-drm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-drm.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-uvm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
